use std::{
    ops::{Deref, DerefMut},
    unreachable,
};

use smallvec::{smallvec, SmallVec};

pub type Container = SmallVec<[u8; 256]>;

/// The bitmap of a glyph.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct Bitmap {
    width: u32,
    height: u32,

    bits: Container,
}

impl Bitmap {
    /// Creates a bitmap of the given size.
    #[inline]
    pub fn new(width: u32, height: u32) -> Self {
        Bitmap {
            width,
            height,
            bits: smallvec![0_u8; (width * height) as usize],
        }
    }

    /// Gets the width.
    #[inline]
    pub fn width(&self) -> u32 {
        self.width
    }

    /// Gets the height.
    #[inline]
    pub fn height(&self) -> u32 {
        self.height
    }

    /// Gets a bit from the map.
    #[inline]
    pub fn get(&self, x: u32, y: u32) -> bool {
        if y >= self.height || x >= self.width {
            panic!("out of bounds");
        }

        match self.bits[((y * self.width) + x) as usize] {
            0xff => true,
            0x0 => false,
            _ => unreachable!("Values of the bitmap can only be 0x0 or 0xff!"),
        }
    }

    /// Sets a bit of the map.
    #[inline]
    pub fn set(&mut self, x: u32, y: u32, value: bool) {
        if y >= self.height || x >= self.width {
            panic!("out of bounds");
        }

        self.bits[((y * self.width) + x) as usize] = if value { 0xff } else { 0 };
    }
}

impl Deref for Bitmap {
    type Target = Container;

    #[inline]
    fn deref(&self) -> &Container {
        &self.bits
    }
}

impl DerefMut for Bitmap {
    #[inline]
    fn deref_mut(&mut self) -> &mut Container {
        &mut self.bits
    }
}
